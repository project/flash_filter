
This module is acting as a filter and it helps to automate Flash content 
embedding process. Core functionality consists of two major parts, the 
first � it works as a content filter and replaces all meta-tags 
[flash:filename.swf] with a corresponding HTML <embed> code and secondly 
it can be enable to auto-display all attachments inline as Flash a 
content.

What�s good in this module is it uses a helper class to determine a 
valid flash content and it extract�s file header with all the bit�s and 
pieces, we are automatically getting dimensions, what is width and 
height. Those values are compared against our settings and if they are 
appropriate we can display Flash inline, if attachment is not a valid 
Flash then we skip it and in case if it�s bigger than our predefined 
maximums we will place a small icon what will act as a link to that 
flash movie on a single page.

This module initially was developed for Drupal 4.7 and will be supported 
for upcoming versions, it might be so that someone want�s to back port 
it for 4.6 please don�t hesitate to ask for support and comments.

For any assistance or help you are welcome to use Skype for contacting 
me, my nick name � veilands.

My greetz goes to:
-	Drupal community
-	To all new members joining
-	To my beloved Olga for supporting me

Any comments and suggestions are welcome please feel free to respond.

Kindly yours,
Tom Veilands a.k.a.(tomsys)